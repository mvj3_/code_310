- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // do something
    [NSThread detachNewThreadSelector:@selector(startImageread:) toTarget:self withObject:indexPath]; // 获取图片
    // do something
}

-(void)startImageread:(NSIndexPath *)indexPath
{                                                                                                      
    UIImage *newimage = ......// 从本地或者网络获取图片
    NSDictionary *cellimage = [NSDictionary dictionaryWithObjectsAndKeys:
                               indexPath, @"indexPath",
                               newimage,@"image",
                               nil];
    [self performSelectorOnMainThread:@selector(_setOCellImage:) withObject:cellimage waitUntilDone:YES];
}

- (void)_setOCellImage:( id )celldata
{
    UIImage *newimage = [celldata objectForKey:@"image"]; //从参数celldata里面拿出来图片
    NSIndexPath *indexPath = [celldata objectForKey:@"indexPath"];
    [self.DataTable cellForRowAtIndexPath:indexPath].imageView.image = newimage;
}